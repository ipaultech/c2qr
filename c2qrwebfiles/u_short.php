<?php
include 'vars.php';

//Get Target folder for all uploads
$targetfolder = $wserveruploadpath;

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    //THIS Code is for Upload from the Clip2QR App
    //Get Session ID from URL
    $sessionident = $_GET['s'];
    if ( $sessionident != null ) {
        //Generate filepath with current session id
        $targetfolderclip = $targetfolder . $sessionident . ".txt" ;
        $targetfolder = $targetfolder . basename( $_FILES['uploaded_file']['name']) ;

        //If uploaded file (from Client) equal to clipboard.txt then...
        if( $_FILES['uploaded_file']['name'] == "clipboard.txt") {
            if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $targetfolderclip))
            {
                #File is successfull uploaded
                echo "The file ". basename( $_FILES['uploaded_file']['name']). " is uploaded";
            }
            else {
                #File is not sucessfull uploaded
                echo "Problem uploading file";
            }
        }
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //THIS Code is for Upload with the Shortcode
    //Get Shortcode ID from URL
    $shortcodeident = $_POST['code'];
    $shortcode_text = $_POST['text'];
    // collect value of input field
    if (strlen($shortcodeident) == 4) {
        if (!preg_match('/[^A-Za-z0-9]/', $shortcodeident)) // '/[^a-z\d]/i' should also work. 
        {
            $identforfilename = strtoupper($shortcodeident);
            echo $identforfilename;
            echo $shortcode_text;

            $targetfolderclip = $targetfolder . $identforfilename . ".txt" ;
            file_put_contents($targetfolderclip, $shortcode_text);
            echo '<br>';
            echo 'success';
        }
        else {
            echo 'Shortcode has invalid characters!';
        }
    }
    else {
        echo 'Shortcode is not 4 digits long!';
    }
}
 ?>
