<?php
#Include Vars
include 'vars.php';

#Starte PHP Sitzung
session_start();
#Übergebe Session_ID an Variable sid
$sid = session_id();

#Define Vars
#Dateipfad für Clipboard Datei
$filepath = $wserverpath .$wserveruploadpath .$sid .'.txt';
#Dateipfad für Clipboard Datei Intern
$filepathinternal = $wserveruploadpath .$sid . '.txt';

#Erstelle den Pfad für den QR-Code anhand der Session ID
$pathqr = $wserverpath .$wserverqrpath .$sid .'.png ';
#Erstelle Internen Pfad für den QR-Code (genutzt für HTML Darstellung)
$pathqrinternal = $wserverqrpath .$sid .'.png';

#Erstelle einen Shortcode (um Text ohne QR-Code einzufügen)
$c2qr_short_string = substr($sid, -4);
$c2qr_short_string = strtoupper($c2qr_short_string);

#Dateipfad für den Shortcode
$short_filepath = $wserverpath .$wserveruploadpath .$c2qr_short_string .'.txt';
#Dateipfad für Clipboard Datei Intern
$short_filepathinternal = $wserveruploadpath .$c2qr_short_string . '.txt';

function textreceived($txtfilepath) {
        #Definiere Clipfiledownload
        $clipfiledownload = fopen($txtfilepath, 'r');
        #HTML Spezifisches
        echo '<p id="text" style="font-size:40px;">';
        #Solange bist es noch Zeilen gibt in dem Text Dokument...
        while (($line = fgets($clipfiledownload)) !== false) {
        #Gebe aktuelle Zeile aus
        echo $line;
        #Erzeuge Absatz (optik und korrektheit in HTML)
        echo '<br>';
        }
        #Weiterer HTML spezifischer kram
        echo '</p>';
        echo '<br>';
        echo '<p><a class="chip" href="index.php">Zurück</a> &nbsp; &nbsp; <a class="chip" href="index.php" target="_blank">Neu</a></p>';
        #Entferne benutzte Session Basierte Dateien
        unlink($txtfilepath);
}

#Falls noch kein Ergebnis kopiert wurde, lade die Seite neu...
if (! file_exists($filepath) && ! file_exists($short_filepath)) {
	#Setze Header auf Refresh
	header("Refresh:3");
}
?>
<html>
<head>
<?php
echo '<title>' .$apptitle .'</title>';
?>
<link rel="shortcut icon" href="ic/favicon.ico" type="image/x-icon">
<link rel="icon" href="ic/favicon32.png" sizes="32x32">
<link rel="icon" href="ic/favicon48.png" sizes="48x48">
<link rel="icon" href="ic/favicon96.png" sizes="96x96">
<link href="/main.css" rel="stylesheet">
<style>
body {
 background-color: rgb(42, 35, 49);
}
</style> 
</head>
<body>
<?php
    #Teste ob bereits ein kopiertes Objekt vorliegt
    #Falls nein:
    if (! file_exists($filepath) && ! file_exists($short_filepath)) {
        #Erstelle einen String im C2QR Format mit Session ID und Hostname
	$c2qrstr = "'<CL2QR<=<UP>" .$srvhostname ."u.php?s=" .$sid ."|=|c2qrWeb<CL2QR>=>UP/>'";
        #Erstelle Pfad für die HTML Seite
	$htmlimgpath = '"' .$wserverqrpath .$sid .'.png"';
	#Falls QR-Code zu dieser Session noch nicht erstellt wurde... Erstelle diesen.
        if (! file_exists($pathqr) ) {
        	$output = shell_exec('qrencode -o '.$pathqr .$c2qrstr);
        }

	# HTML Content für die Darstellung
        echo '<p class="aligncenter">';
        echo '<b style="font-size:80px; color: #ff6600">Clip2QR</b>';
        echo '<br>';
        echo '<br>';
	#Ausgabe des Bildes mit dem Session Based String
        echo '  <img src=' .$htmlimgpath .'alt="centered image" width="600" height="auto"/>';
        echo '<br>';
        echo '<p class="aligncenter">';
        echo '<b style="font-size:60px;color:#ff0000;"># ' .$c2qr_short_string .' #</b>';
        echo '<div class="chip-container">';
        echo '    <div class="chip"><a href="https://qr.antopie.org">LibreQR</a></div>';
        echo '    <div class="chip"><a href="send.html"># SEND (Beta) #</a></div>';
        echo '    <div class="chip"><a href="https://gitlab.com/ipaultech/c2qr">Source</a></div>';
        echo '</div>';
        echo '<p class="aligncenter">';
        echo '<b style="font-size:25px;color:#b951dc;">Weil tippen von gestern ist... Ein Tool von Paul B.</b>';
        echo '<br>';
        echo '<b style="font-size:25px;color:#b951dc;">v1.4 (2023-02-14)</b>';
        echo '</p>';
    }
    #Falls kopiertes Objekt besteht dann:
    else {
        if (file_exists($filepath) == true) {
                textreceived($filepathinternal);
                if ( file_exists($pathqr) ) {
                        unlink($pathqrinternal);
                }
        }
        if (file_exists($short_filepath) == true) {
                textreceived($short_filepathinternal);
        }
    }
?>
<style>
.aligncenter {
   text-align: center;
}
</style>
</body>
</html>
