FROM php:apache-bullseye
WORKDIR /var/www/html

#Define env vars
ARG HOSTNAME
ARG TABTITLE

#Copy dependencys
COPY deps /deps

#Install dependencys
RUN dpkg -i /deps/libqrencode4_4.1.1-1_amd64.deb
RUN dpkg -i /deps/libpng16-16_1.6.37-3_amd64.deb
RUN dpkg -i /deps/qrencode_4.1.1-1_amd64.deb

#Copy Webserver Folder
COPY c2qrwebfiles /var/www/html
#Change Rights
RUN chmod -R 777 /var/www/html

#Copy start script to image
COPY start.sh /start.sh
#Setting permissions
RUN chmod 700 /start.sh
#Start the script on every bootup
CMD ["/start.sh"]

EXPOSE 80
