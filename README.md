# Repo moved to https://codeberg.org/berzdev/c2qr
# Clipboard 2 QR (C2QR) Web

C2QR is a tool for Copy Text from your Android to a PC with a QR-Code.

I created the project because it annoys me to type text from my phone onto someone else's computer.

## Preview
![image](/demo.png)

Test it if you like on my [Public Instance](https://c2qr.berz.dev).

## Installation

This tool is dockerized!

Download:
```sh
git clone https://gitlab.com/ipaultech/c2qr.git
```

Modify for your setup with vi:
```sh
vi docker-compose.yml
```

Starting
```sh
docker compose up -d
```

Now should your C2QR instance be available at port 80 (if you haven't changed it).

## Potential Features for the future

* File Transfer Support
* Better Android App (release is should be done soon)
* Zero Trust Encryption (That the Server can't access the Clipboard information)
* Browser Addon?
