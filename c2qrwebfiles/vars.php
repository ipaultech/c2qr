<?php
#This File includes runtime vars, that'll be used globally in all scripts.
#RUNTIME VARS

//HOSTNAME [SERVER Adress] (for QR-Code generation)
$srvhostname = getenv('HOSTNAME');

//Webserver Path
$wserverpath = '/var/www/html/';

//Folder for Uploads (Files, Text, and more...)
$wserveruploadpath = 'uploads/';

//Folder for generated QR-Codes
$wserverqrpath = 'qrs/';


#Design Stuff
//Website Title
$apptitle = getenv('TABTITLE');

?>
