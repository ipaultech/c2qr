<?php
include 'vars.php';

//THIS Code is for Upload from the Clip2QR App
//Get Session ID from URL
$sessionident = $_GET['s'];

//Get Target folder for all uploads
$targetfolder = $wserveruploadpath;

//Generate filepath with current session id
$targetfolderclip = $targetfolder . $sessionident . ".txt" ;
$targetfolder = $targetfolder . basename( $_FILES['uploaded_file']['name']) ;

//If uploaded file (from Client) equal to clipboard.txt then...
if( $_FILES['uploaded_file']['name'] == "clipboard.txt") {
    if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $targetfolderclip))
    {
        #File is successfull uploaded
        echo "The file ". basename( $_FILES['uploaded_file']['name']). " is uploaded";
    }
    else {
        #File is not sucessfull uploaded
        echo "Problem uploading file";
    }
}
 ?>
