while true
#Start Apache Server
apache2-foreground &
do
	#Wait X seconds before delete old QR Codes and Uploads
	sleep 600
	rm /var/www/html/qrs/*
	rm /var/www/html/uploads/*
done
